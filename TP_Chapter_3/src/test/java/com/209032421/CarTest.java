package com.209032421;

import static org.junit.Assert.assertTrue;
import junit.framework.TestCase;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class CarTest extends TestCase {
    List<CarInterface> carList = new ArrayList();
    Set<CarInterface> hashSet = new HashSet();
    Map<String, CarInterface> hashMap = new HashMap();

    Trolley shoprite = new Trolley("Tortuose", "Slowster", 2011);
    Trolley pnp = new Trolley("slug", "Speedster", 2013);
    Trolley woolworths = new Trolley("Snail", "Slickster", 2015);

    Toyota corolla = new Toyota("Toyota", "20v", 2011);
    Toyota conquest = new Toyota("Toyota", "16v", 2013);
    Toyota camry = new Toyota("Toyota", "2.0", 2015);

    Porsche p = new Porsche("Porsche", "boxtser", 2011);
    Porsche p2 = new Porsche("Porsche", "911", 2013);
    Porsche p3 = new Porsche("Porsche", "Carrera GT", 2015);

 //Test object variation
    public void testListObjectNotSame() throws Exception {
        carList.add(0, shoprite);
        carList.add(1, corolla);

        assertNotSame(carList.get(0).engine(), carList.get(1).engine());
    }


//test Set null
    public void testHashSetOjectNotNull() throws Exception {
        assertNull(hashSet);
    }


//Test SetNot Null
    public void testHashSetOjectNotNull() throws Exception {
        hashSet.add(conquest);
        hashSet.add(woolworths);

        assertNotNull(hashSet);
    }

//Test Null Hashmap
    public void testHashMapObjectNull() throws Exception {
        hashMap.put("2", pnp);
        assertNull(hashMap.get("3"));
    }


    //Test Not Null Hash
    public void testHashMapNull() throws Exception {
        hashMap.put("2", pnp);
        assertNotNull(hashMap.get("2"));
    }
}