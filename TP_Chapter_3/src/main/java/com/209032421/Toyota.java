package com.209032421;


public class Toyota extends Car{

    public Toyota(String cMake, String cModel, int cYear)
    {
        super(cModel, cModel, cYear);
    }

    public String engine(){
        return "Vroom Vroom in Toyota";
    }
}
